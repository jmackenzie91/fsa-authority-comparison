package main

import (
	"database/sql"
	"github.com/sirupsen/logrus"
	"no_vcs/me/go-modules/api"
	"no_vcs/me/go-modules/repositories"

	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"os"
)

func main() {
	l := setupLogger()
	db, err := setupConnection()

	v1 := api.NewAPI(db, l, setupRoutes)
	err = v1.Run(80)

	if err != nil {
		l.Fatal(err)
	}
}

func setupConnection() (*sql.DB, error) {
	connString := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?parseTime=true",
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASS"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_NAME"),
	)

	db, err := sql.Open("mysql", connString)
	if err != nil {
		return nil, err
	}
	db.SetMaxIdleConns(5)
	db.SetMaxOpenConns(5)

	return db, nil
}

func setupRoutes(a *api.API) {

	db, _ := setupConnection()

	authRepo := repositories.NewAuthRepo(db)
	establishmentRepo := repositories.NewEstablishmentsRepo(db)

	a.Router = mux.NewRouter().StrictSlash(true)

	a.Router.HandleFunc(
		"/v1",
		a.InitWelcomeHandler(),
	).Methods("GET")

	a.Router.HandleFunc("/", a.InitHome(authRepo))
	a.Router.HandleFunc("/compare", a.InitCompare(establishmentRepo))
}

func setupLogger() *logrus.Logger {
	lvl, err := logrus.ParseLevel(os.Getenv("LOG_LEVEL"))

	if err != nil {
		fmt.Errorf("%s", err)
		lvl = logrus.DebugLevel
	}

	l := logrus.New()
	l.SetLevel(lvl)
	l.SetFormatter(&logrus.JSONFormatter{})
	return l
}
