package models

type Authority struct {
	LocalAuthorityId   int    `json:"LocalAuthorityId"`
	Name               string `json:"Name"`
	EstablishmentCount int    `json:"EstablishmentCount"`
}

type Establishment struct {
	BusinessName string `json:"BusinessName"`
	RatingValue  string `json:"RatingValue"`
	AuthorityId  int
}

type AuthoritiesResponse struct {
	Authorities []Authority `json:"authorities"`
}

type EstablishmentsResponse struct {
	Establishments []Establishment `json:"establishments"`
}
