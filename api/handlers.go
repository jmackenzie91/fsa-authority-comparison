package api

import (
	"html/template"
	"net/http"
	"no_vcs/me/go-modules/models"
	"no_vcs/me/go-modules/repositories"
	"no_vcs/me/go-modules/transformers"
	"strconv"
)

func (a API) InitWelcomeHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Welcome"))
	}
}

func (a API) InitHome(authRepo repositories.Authorities) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		authorities, _ := authRepo.GetAll()

		t, _ := template.ParseFiles("templates/home.html")
		t.Execute(w, struct {
			Authorities []models.Authority
		}{
			Authorities: authorities,
		})
	}
}

func (a API) InitCompare(establishmentRepo repositories.Establishments) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		r.ParseForm()

		authOneId, _ := strconv.Atoi(r.Form.Get("authority_one"))
		authTwoId, _ := strconv.Atoi(r.Form.Get("authority_two"))

		authorityOneEstablishments, err := establishmentRepo.FindByAuthorityId(authOneId)

		if err != nil {
			panic(err)
		}
		authorityTwoEstablishments, err := establishmentRepo.FindByAuthorityId(authTwoId)

		if err != nil {
			panic(err)
		}

		t, _ := template.ParseFiles("templates/compare.html")
		t.Execute(w, struct {
			AuthorityOneSummary transformers.Summary
			AuthorityTwoSummary transformers.Summary
		}{
			AuthorityOneSummary: transformers.EstablishmentsSummary(authorityOneEstablishments),
			AuthorityTwoSummary: transformers.EstablishmentsSummary(authorityTwoEstablishments),
		})
	}
}
