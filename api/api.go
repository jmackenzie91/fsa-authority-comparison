package api

import (
	"database/sql"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net/http"
)

// API is a struct for a typical api, can possibly be reused for v2
type API struct {
	DB     *sql.DB
	Logger *logrus.Logger
	Router *mux.Router
}

// NewAPI constructor method for API struct
func NewAPI(db *sql.DB, l *logrus.Logger, f func(*API)) API {

	s := API{
		DB:     db,
		Logger: l,
	}
	f(&s)

	return s
}

// Run runs the server
func (s API) Run(port int) error {
	if err := http.ListenAndServe(fmt.Sprintf(":%d", port), s.Router); err != nil {
		return err
	}
	return nil
}
