package repositories

import (
	"database/sql"
	"no_vcs/me/go-modules/models"
)

type Authorities struct {
	DB *sql.DB
}

func NewAuthRepo(db *sql.DB) Authorities {
	return Authorities{
		DB: db,
	}
}

func (a Authorities) Truncate() error {
	_, err := a.DB.Exec(`DELETE FROM authorities;`)
	return err
}

func (a Authorities) Create(authority models.Authority) (int64, error) {
	res, err := a.DB.Exec(
		`INSERT INTO authorities (local_authority_id, name, establishment_count) VALUES (?, ?, ?);`,
		authority.LocalAuthorityId,
		authority.Name,
		authority.EstablishmentCount,
	)

	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()

	if err != nil {
		return 0, err
	}

	return id, nil
}

func (a Authorities) GetAll() ([]models.Authority, error) {
	res, err := a.DB.Query("SELECT local_authority_id, name, establishment_count FROM authorities")

	if err != nil {
		return nil, err
	}
	defer res.Close()

	authorities := []models.Authority{}

	for res.Next() {
		var authority models.Authority
		res.Scan(&authority.LocalAuthorityId, &authority.Name, &authority.EstablishmentCount)

		if res.Err() != nil {

			return nil, res.Err()
		}
		authorities = append(authorities, authority)
	}

	return authorities, nil
}
