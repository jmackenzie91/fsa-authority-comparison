package repositories

import (
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/magiconair/properties/assert"
	"no_vcs/me/go-modules/models"
	"testing"
)

func TestAuthorities_Truncate(t *testing.T) {
	testCases := []struct {
		Name           string
		SetupQueries   func(*sqlmock.Sqlmock)
		ExpectedOutput error
	}{
		{
			Name: "Insert successful",
			SetupQueries: func(mock *sqlmock.Sqlmock) {
				m := *mock
				m.ExpectExec("DELETE FROM authorities;").WillReturnResult(sqlmock.NewResult(1, 1))
			},

			ExpectedOutput: nil,
		},
		{
			Name: "Check error has been thrown",
			SetupQueries: func(mock *sqlmock.Sqlmock) {
				m := *mock
				m.ExpectExec("DELETE FROM authorities;").WillReturnError(errors.New("some error"))
			},
			ExpectedOutput: errors.New("some error"),
		},
	}

	for _, tc := range testCases {
		// Arrange
		db, mockDB, err := sqlmock.New()
		if err != nil {
			panic(err)
		}

		tc.SetupQueries(&mockDB)

		repo := Authorities{
			DB: db,
		}

		// Act
		output := repo.Truncate()

		// Assert
		assert.Equal(t, tc.ExpectedOutput, output, tc.Name)
	}
}

func TestAuthorities_Create(t *testing.T) {
	testCases := []struct {
		Name           string
		SetupQueries   func(*sqlmock.Sqlmock)
		InputAuthority models.Authority
		ExpectedOutput int64
		ExpectedError  error
	}{
		{
			Name: "Insert successful",
			SetupQueries: func(mock *sqlmock.Sqlmock) {
				m := *mock
				m.ExpectExec(
					"INSERT INTO authorities \\(local_authority_id, name, establishment_count\\) VALUES \\(\\?, \\?, \\?\\);",
				).WithArgs(22, "name", 123).WillReturnResult(sqlmock.NewResult(1, 1))
			},
			InputAuthority: models.Authority{
				LocalAuthorityId:   22,
				Name:               "name",
				EstablishmentCount: 123,
			},
			ExpectedOutput: 1,
			ExpectedError:  nil,
		},
		{
			Name: "Check error has been thrown",
			SetupQueries: func(mock *sqlmock.Sqlmock) {
				m := *mock
				m.ExpectExec(
					"INSERT INTO authorities \\(local_authority_id, name, establishment_count\\) VALUES \\(\\?, \\?, \\?\\);",
				).WillReturnError(errors.New("some error"))
			},
			InputAuthority: models.Authority{},
			ExpectedOutput: 0,
			ExpectedError:  errors.New("some error"),
		},
	}

	for _, tc := range testCases {
		// Arrange
		db, mockDB, err := sqlmock.New()
		if err != nil {
			panic(err)
		}

		tc.SetupQueries(&mockDB)

		repo := Authorities{
			DB: db,
		}

		// Act
		output, outputErr := repo.Create(tc.InputAuthority)

		// Assert
		assert.Equal(t, tc.ExpectedOutput, output, tc.Name)
		assert.Equal(t, tc.ExpectedError, outputErr, tc.Name)
	}
}

func TestAuthorities_GetAll(t *testing.T) {
	testCases := []struct {
		Name           string
		SetupQueries   func(*sqlmock.Sqlmock)
		ExpectedOutput []models.Authority
		ExpectedError  error
	}{
		{
			Name: "GetAll successful",
			SetupQueries: func(mock *sqlmock.Sqlmock) {
				m := *mock
				m.ExpectQuery(
					"SELECT local_authority_id, name, establishment_count FROM authorities",
				).WillReturnRows(sqlmock.NewRows([]string{"local_authority_id", "name", "establishment_count"}).
					AddRow(1, "some authority", 123))
			},
			ExpectedOutput: []models.Authority{
				{
					LocalAuthorityId:   1,
					Name:               "some authority",
					EstablishmentCount: 123,
				},
			},
			ExpectedError: nil,
		},
		{
			Name: "GetAll throws error",
			SetupQueries: func(mock *sqlmock.Sqlmock) {
				m := *mock
				m.ExpectQuery(
					"SELECT local_authority_id, name, establishment_count FROM authorities",
				).WillReturnError(errors.New("some error"))
			},
			ExpectedOutput: nil,
			ExpectedError:  errors.New("some error"),
		},
	}

	for _, tc := range testCases {
		// Arrange
		db, mockDB, err := sqlmock.New()
		if err != nil {
			panic(err)
		}

		tc.SetupQueries(&mockDB)

		repo := Authorities{
			DB: db,
		}

		// Act
		output, outputErr := repo.GetAll()

		// Assert
		assert.Equal(t, tc.ExpectedOutput, output, tc.Name)
		assert.Equal(t, tc.ExpectedError, outputErr, tc.Name)
	}
}
