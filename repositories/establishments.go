package repositories

import (
	"database/sql"
	"no_vcs/me/go-modules/models"
)

type Establishments struct {
	DB *sql.DB
}

func NewEstablishmentsRepo(db *sql.DB) Establishments {
	return Establishments{
		DB: db,
	}
}

func (e Establishments) Truncate() error {
	_, err := e.DB.Exec(`DELETE FROM establishments;`)
	return err
}

func (e Establishments) Create(establishment models.Establishment) (int64, error) {
	res, err := e.DB.Exec(
		`INSERT INTO establishments (business_name, rating_value, authority_id) VALUES (?, ?, ?);`,
		establishment.BusinessName,
		establishment.RatingValue,
		establishment.AuthorityId,
	)

	if err != nil {
		return 0, err
	}

	id, err := res.LastInsertId()

	if err != nil {
		return 0, err
	}

	return id, nil
}

func (e Establishments) FindByAuthorityId(id int) ([]models.Establishment, error) {
	res, err := e.DB.Query(`SELECT business_name, rating_value, authority_id FROM establishments WHERE authority_id = ?;`, id)

	if err != nil {
		return nil, err
	}

	defer res.Close()

	var establishments []models.Establishment
	for res.Next() {
		var establishment models.Establishment
		if err := res.Scan(&establishment.BusinessName, &establishment.RatingValue, &establishment.AuthorityId); err != nil {
			return nil, err
		}
		establishments = append(establishments, establishment)
	}

	return establishments, nil
}
