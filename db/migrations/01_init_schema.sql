CREATE DATABASE fsa_authority_comparison;

USE fsa_authority_comparison;

CREATE TABLE authorities (
  id INT AUTO_INCREMENT PRIMARY KEY,
  local_authority_id INT NOT NULL,
	name VARCHAR(255) NOT NULL,
	establishment_count INT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE UNIQUE INDEX ix_authorities ON `authorities` (local_authority_id);

CREATE TABLE establishments (
  id INT AUTO_INCREMENT PRIMARY KEY,
	business_name VARCHAR(255) NOT NULL,
	rating_value VARCHAR(255) NOT NULL,
	authority_id INT NOT NULL,
	FOREIGN KEY(authority_id) REFERENCES authorities(local_authority_id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
