package fsa_test

import (
	"context"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/httptest"
	"no_vcs/me/go-modules/fsa"
	"no_vcs/me/go-modules/models"
	"testing"
	"time"
)

func TestFsaClient_GetAuthorities(t *testing.T) {
	testCases := []struct {
		Name                 string // This will be displayed if a test errors
		ResponseBody         string // The body of our mocked response
		ResponseStatusCode   int
		HangTime             time.Duration // The amount of time in which the request will hand (used to test the timeout scenario)
		ExpectedClientOutout []models.Authority
		ExpectedClientError  error
	}{
		{
			Name:               "GET Authorities/basic/ returns results",
			ResponseStatusCode: http.StatusOK,
			ResponseBody:       "../testdata/fsa/authorities/basic/200.json",
			ExpectedClientOutout: []models.Authority{
				{
					LocalAuthorityId:   197,
					Name:               "Aberdeen City",
					EstablishmentCount: 1802,
				},
				{
					LocalAuthorityId:   198,
					Name:               "Aberdeenshire",
					EstablishmentCount: 2109,
				},
			},
		},
		//{
		//	Name:                "GET Authorities/basic/ returns gracefully when timeout",
		//	ResponseStatusCode:  http.StatusRequestTimeout,
		//	ResponseBody:        "",
		//	HangTime:            time.Second * 3,
		//	ExpectedClientError: TimeoutError,
		//},
		{
			Name:                 "GET Authorities/basic/ throws 500",
			ResponseStatusCode:   http.StatusInternalServerError,
			ResponseBody:         "",
			ExpectedClientOutout: nil,
			ExpectedClientError:  fsa.Err500,
		},
	}

	for _, tc := range testCases {
		// Arrange

		h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.ResponseStatusCode)
			time.Sleep(tc.HangTime)
			f, _ := ioutil.ReadFile(tc.ResponseBody)
			w.Write(f)
		})

		s := httptest.NewServer(h)
		defer s.Close()

		mockCli := &http.Client{
			Transport: &http.Transport{
				DialContext: func(_ context.Context, network, _ string) (net.Conn, error) {
					return net.Dial(network, s.Listener.Addr().String())
				},
			},
			Timeout: time.Second * 3,
		}

		cli := fsa.NewClient()
		cli.Client = mockCli

		// Act
		res, err := cli.GetAuthorities()

		// Assert
		assert.Equal(t, tc.ExpectedClientOutout, res, tc.Name)
		assert.Equal(t, tc.ExpectedClientError, err, tc.Name)
	}
}

func TestFsaClient_GetEstablishments(t *testing.T) {
	testCases := []struct {
		Name                 string // This will be displayed if a test errors
		ResponseBody         string // The body of our mocked response
		ResponseStatusCode   int
		HangTime             time.Duration // The amount of time in which the request will hand (used to test the timeout scenario)
		ExpectedClientOutout []models.Establishment
		ExpectedClientError  error
	}{
		{
			Name:               "GET Establishments/ returns results",
			ResponseStatusCode: http.StatusOK,
			ResponseBody:       "../testdata/fsa/establishments/200.json",
			ExpectedClientOutout: []models.Establishment{
				{
					BusinessName: "'Cherished' Coffee Shop @ Hayes Green Community Centre",
					RatingValue:  "5",
				},
				{
					BusinessName: "3663 (Staff Canteen)",
					RatingValue:  "5",
				},
			},
		},
		//{
		//	Name:                "GET Authorities/basic/ returns gracefully when timeout",
		//	ResponseStatusCode:  http.StatusRequestTimeout,
		//	ResponseBody:        "",
		//	HangTime:            time.Second * 3,
		//	ExpectedClientError: TimeoutError,
		//},
		{
			Name:                 "GET Authorities/basic/ throws 500",
			ResponseStatusCode:   http.StatusInternalServerError,
			ResponseBody:         "",
			ExpectedClientOutout: nil,
			ExpectedClientError:  fsa.Err500,
		},
	}

	for _, tc := range testCases {
		// Arrange

		h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.ResponseStatusCode)
			time.Sleep(tc.HangTime)
			f, _ := ioutil.ReadFile(tc.ResponseBody)
			w.Write(f)
		})

		s := httptest.NewServer(h)
		defer s.Close()

		mockCli := &http.Client{
			Transport: &http.Transport{
				DialContext: func(_ context.Context, network, _ string) (net.Conn, error) {
					return net.Dial(network, s.Listener.Addr().String())
				},
			},
			Timeout: time.Second * 3,
		}

		cli := fsa.NewClient()
		cli.Client = mockCli

		// Act
		res, err := cli.GetEstablishments(1)

		// Assert
		assert.Equal(t, tc.ExpectedClientOutout, res, tc.Name)
		assert.Equal(t, tc.ExpectedClientError, err, tc.Name)
	}
}
