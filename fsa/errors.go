package fsa

import (
	"errors"
)

var Err500 = errors.New("500 Internal Server error returned")
var ErrTimeout = errors.New("Server timed out")
