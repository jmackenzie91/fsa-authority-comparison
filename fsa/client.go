package fsa

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"no_vcs/me/go-modules/models"
	"time"
)

const BaseUrl = "http://api.ratings.food.gov.uk/"

type FsaClient struct {
	Client interface {
		Do(req *http.Request) (*http.Response, error)
	}
}

func NewClient() FsaClient {
	return FsaClient{
		Client: &http.Client{
			Timeout: time.Second * 10,
		},
	}
}

func (c FsaClient) GetAuthorities() ([]models.Authority, error) {
	r, _ := http.NewRequest("GET", fmt.Sprintf("%s/Authorities/basic", BaseUrl), nil)
	r.Header.Set("x-api-version", "2")
	res, _ := c.Client.Do(r)

	switch res.StatusCode {
	case http.StatusInternalServerError:
		return nil, Err500
	}

	var authoritiesResponse models.AuthoritiesResponse
	parseJSONToStruct(res, &authoritiesResponse)
	return authoritiesResponse.Authorities, nil
}

func (c FsaClient) GetEstablishments(id int) ([]models.Establishment, error) {
	r, _ := http.NewRequest("GET", fmt.Sprintf("%s/Establishments?localAuthorityId=%d", BaseUrl, id), nil)
	r.Header.Set("x-api-version", "2")
	res, _ := c.Client.Do(r)

	switch res.StatusCode {
	case http.StatusInternalServerError:
		return nil, Err500
	}

	var establishmentsResponse models.EstablishmentsResponse
	parseJSONToStruct(res, &establishmentsResponse)

	for i := 0; i < len(establishmentsResponse.Establishments); i++ {
		establishmentsResponse.Establishments[i].AuthorityId = id
	}

	return establishmentsResponse.Establishments, nil
}

func parseJSONToStruct(r *http.Response, i interface{}) error {
	resBody, _ := ioutil.ReadAll(r.Body)
	return json.Unmarshal(resBody, &i)
}
