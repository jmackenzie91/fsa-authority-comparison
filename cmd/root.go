package main

import (
	"fmt"
	"github.com/spf13/cobra"
	"no_vcs/me/go-modules/cmd/index"
	"os"
)

var rootCmd = &cobra.Command{
	Use:   "fsa-authority-comparison",
	Short: "A collection of commands to help set up FSA app.",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(`Hello and welcome to fsa-authority-comparison console. 
Please use --help to see a list of options.`)
	},
}

func main() {
	rootCmd.AddCommand(index.Index)
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
