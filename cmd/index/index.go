package index

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"no_vcs/me/go-modules/fsa"
	"no_vcs/me/go-modules/models"
	"no_vcs/me/go-modules/repositories"
	"os"
)

// Index represents the reindex command
var Index = &cobra.Command{
	Use:   "index",
	Short: "Queries FSA api and indexes database",
	Run:   run,
}

var authRepo repositories.Authorities
var establishmentRepo repositories.Establishments
var fsaClient fsa.FsaClient

var input = make(chan models.Authority)
var output = make(chan Result)
var completed = make(chan struct{})
var logger *logrus.Logger

func run(cmd *cobra.Command, args []string) {

	// Set up DB + repos + client
	db, err := setupConnection()
	CheckError(err)

	authRepo = repositories.NewAuthRepo(db)
	establishmentRepo = repositories.NewEstablishmentsRepo(db)
	fsaClient = fsa.NewClient()

	// grab all authorities
	authorities, err := fsaClient.GetAuthorities()
	CheckError(err)

	fmt.Println("Truncate authorities table")
	err = authRepo.Truncate()
	CheckError(err)

	fmt.Println("Truncate establishments table")
	err = establishmentRepo.Truncate()
	CheckError(err)

	// watch output channel
	go watchOutputChannel(len(authorities), output, completed)

	// set up 4 go routines to process authorities
	for i := 1; i <= 4; i++ {
		go ProcessAuthority(input, output)
	}

	for _, authority := range authorities {

		_, err := authRepo.Create(authority)
		if err != nil {
			panic(err) //todo graceful
		}

		input <- authority
	}

	<-completed
	fmt.Println("All establishments successfully indexed")
}

func setupConnection() (*sql.DB, error) {
	connString := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?parseTime=true",
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASS"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_NAME"),
	)

	db, err := sql.Open("mysql", connString)
	if err != nil {
		return nil, err
	}
	db.SetMaxIdleConns(5)
	db.SetMaxOpenConns(5)

	return db, nil
}

func ProcessAuthority(input chan models.Authority, output chan Result) {
	for {
		auth := <-input

		if auth.Name == "" { // empty struct passed from channel, not too sure why this happens
			continue
		}
		establishments, err := fsaClient.GetEstablishments(auth.LocalAuthorityId)

		if err != nil {
			panic(err) //todo graceful
		}

		for _, establishment := range establishments {
			_, err := establishmentRepo.Create(establishment)

			if err != nil {
				panic(err) //todo graceful
			}
		}
		output <- Result{
			AuthorityName:          auth.Name,
			NumberOfEstablishments: len(establishments),
		}
	}
}

func watchOutputChannel(count int, output chan Result, completed chan struct{}) {
	for i := 0; i < count; i++ {
		r := <-output
		fmt.Printf(
			"successfully indexed %d establishments for authority: %s\n",
			r.NumberOfEstablishments,
			r.AuthorityName,
		)
	}
	completed <- struct{}{}
}

func CheckError(err error) {
	if err != nil {
		fmt.Fprint(os.Stderr, err)
		os.Exit(1)
	}
}

type Result struct {
	AuthorityName          string
	NumberOfEstablishments int
}
