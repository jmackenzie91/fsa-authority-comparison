container=fsa-authority-comparison-app

up:
	docker-compose up -d

build:
	docker-compose rm -vsf
	docker-compose down -v --remove-orphans
	docker-compose build

down:
	docker-compose down

restart-app:
	docker-compose restart ${container}

jumpin:
	docker-compose exec ${container} bash

index:
	docker-compose exec ${container} ./cmd/root index

test:
	go test ./...

tail-logs:
	docker-compose logs -f ${container}
