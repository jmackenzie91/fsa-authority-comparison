# FSA Authority Comparison App

This is an app to compare the ratings of various establishments under its authority.

## Setting Infrastructure

Copy the .env file

`cp example.env .env`

Download the dependencies

`go mod vendor`

Build `make build` and run `make up` the app. If this is your initial build
please see below.

## Indexing Establishment Data

To save querying the FSA api every time we want to gather establishment data
I decided to index the FSA api into a mysql DB. The first time this project is borught
up you will need to run the indexer. You can by `make index`.


## Using the app

To run the app locally run `make up`, the site will now be accessible on http://0.0.0.0:8079/

Have fun!
