FROM golang

ENV GO111MODULE=on

COPY . /go/src

WORKDIR /go/src

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./cmd/root ./cmd/
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -mod=vendor -o fsa-authority-comparison
CMD ["/go/src/fsa-authority-comparison"]
