package transformers

import (
	"github.com/magiconair/properties/assert"
	"no_vcs/me/go-modules/models"
	"testing"
)

func TestEstablishmentsSummary(t *testing.T) {
	testCases := []struct {
		Input          []models.Establishment
		ExpectedOutput Summary
	}{
		{
			Input: []models.Establishment{
				{
					RatingValue: "Pass",
				},
				{
					RatingValue: "Pass",
				},
				{
					RatingValue: "5",
				},
				{
					RatingValue: "4",
				},
			},
			ExpectedOutput: Summary{
				Pass: 2,
				Five: 1,
				Four: 1,
			},
		},
	}

	for i, tc := range testCases {
		// Arrange
		// Act
		output := EstablishmentsSummary(tc.Input)
		// Assert
		assert.Equal(t, tc.ExpectedOutput, output, string(i))
	}
}
