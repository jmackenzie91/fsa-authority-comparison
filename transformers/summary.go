package transformers

import "no_vcs/me/go-modules/models"

type Summary struct {
	Pass                int
	PassAndEatSafe      int
	ImprovementRequired int
	AwaitingInspection  int
	AwaitingPublication int
	Exempt              int
	Five                int
	Four                int
	Three               int
	Two                 int
	One                 int
	Zero                int
}

func EstablishmentsSummary(establishments []models.Establishment) Summary {
	summ := Summary{}
	for _, establishment := range establishments {
		switch establishment.RatingValue {
		case "Pass":
			summ.Pass++
		case "Pass and Eat Safe":
			summ.PassAndEatSafe++
		case "Improvement Required":
			summ.ImprovementRequired++
		case "Awaiting Inspection":
			summ.AwaitingInspection++
		case "Exempt":
			summ.Exempt++
		case "5":
			summ.Five++
		case "4":
			summ.Four++
		case "3":
			summ.Three++
		case "2":
			summ.Two++
		case "1":
			summ.One++
		case "0":
			summ.Zero++
		case "AwaitingInspection":
			summ.AwaitingInspection++
		case "AwaitingPublication":
			summ.AwaitingPublication++
		}
	}
	return summ
}
